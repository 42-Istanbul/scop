NAME = scop
SRCS = main.cpp \
		scop.cpp \
		window_handling.cpp \
		events_handling.cpp
SRC_GLAD = ./libraries/glad/src/glad.cpp
OBJS = $(SRCS:.o=.cpp)

CC = c++
RM = rm -rf
CXXFLAGS = -Wall -Wextra -Werror
INCLUDES = -I./include -I./libraries/glad/include -I./libraries/glfw/include
UNAME = $(shell uname)
ifeq ($(UNAME), Linux)
	LFLAGS = -lglfw -lGL -lm -lX11 -lpthread -lXrandr -lXi -ldl
else
	INCLUDES += -I./libraries
	LFLAGS = -L./libraries/glfw/lib -lglfw -lm -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo
endif

all: $(NAME)

debug: CXXFLAGS += -g
debug: $(NAME)

address: CXXFLAGS += -fsanitize=address -g
address: re

$(NAME): $(OBJS)
	$(CC) $(CXXFLAGS) $(SRC_GLAD) $(OBJS) -o $(NAME) $(LFLAGS) $(INCLUDES)

%.o: %.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@ $(INCLUDES)

clean:
	$(RM) *.o

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re debug address
