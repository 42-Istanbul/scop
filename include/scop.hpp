#pragma once

// C++ std libraries
# include <iostream>
# include <fstream>
# include <cstdlib>
# include <string>
# include <vector>
# include <algorithm>

// OpengGL libraries
# include <glad/glad.h>
# include <GLFW/glfw3.h>
# include <glm/glm.hpp>
# include <glm/gtc/matrix_transform.hpp>
# include <glm/gtc/type_ptr.hpp>

// Local header
# include "vec.hpp"

# define WIDTH 800
# define HEIGHT 600

# ifdef __APPLE__
#  define PATH ".."
# else
#  define PATH "."
# endif

std::vector<scop::vec3>	loadObj(std::string filename);

// Window Handling
GLFWwindow	*initWin(void);

// Events Handling
void	processInput(GLFWwindow *win);
