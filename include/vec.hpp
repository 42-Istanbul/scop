#pragma once

# include <iostream>
# include <cmath>

namespace scop
{
	template<typename T>
	class vec
	{
	private:
		T	m_X;
		T	m_Y;
		T	m_Z;

	public:
		vec() : m_X(0), m_Y(0), m_Z(0) {}
		vec(T x, T y) : m_X(x), m_Y(y), m_Z(0) {}
		vec(T x, T y, T z) : m_X(x), m_Y(y), m_Z(z) {}
		vec(const vec &copyObj)
		{
			*this = copyObj;
		}
		vec	&operator=(const vec &copyObj)
		{
			if (this != &copyObj)
			{
				m_X = copyObj.m_X;
				m_Y = copyObj.m_Y;
				m_Z = copyObj.m_Z;
			}
			return (*this);
		}
		~vec() {}

		/* Getter and Setter functions */
		const T	getX(void) const { return (m_X); }
		const T	getY(void) const { return (m_Y); }
		const T	getZ(void) const { return (m_Z); }
		const T	getLen(void) const { return (std::sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z)); }

		void	setX(T x) { m_X = x; }
		void	setY(T y) { m_Y = y; }
		void	setZ(T z) { m_Z = z; }

		/* Operator overloadings */
		vec	operator+(const vec &b) const { return (vec(m_X + b.m_X, m_Y + b.m_Y, m_Z + b.m_Z)); }
		vec	operator-(const vec &b) const { return (vec(m_X - b.m_X, m_Y - b.m_Y, m_Z - b.m_Z)); }
		vec	operator*(T var) { return (vec(m_X * var, m_Y * var, m_Z * var)); }
		vec	operator/(T var) { return (vec(m_X / var, m_Y / var, m_Z / var)); }
		T		operator*(const vec &b) const { return (m_X * b.m_X + m_Y * b.m_Y + m_Z * b.m_Z); }

		/* Other functions */
		vec	normalize() const
		{
			T	len = getLen();
			if (len == 0)
				return (*this);
			else
				return (vec(m_X, m_Y, m_Z) / len);
		}
	};

	template<typename T>
	std::ostream	&operator<<(std::ostream &out, const vec<float> &vec)
	{
		out << "(" << vec.getX() << ", " << vec.getY() << ", " << vec.getZ() << ")";
		return (out);
	}

	typedef vec<float>	vec3;
	typedef vec<int>	ivec3;
}
