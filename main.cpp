#include "scop.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

GLuint	loadShaders(void)
{
	std::string	temp = "";
	std::string	src = "";

	// Check if shaders are okay
	GLint	success;
	char 	infoLog[512];

	// 1. Vertex Shader
	char	*path = realpath(PATH, NULL);
	std::ifstream	vertex_file(std::string(path) + std::string("/vertex_core.glsl"));
	free(path);
	if (vertex_file.is_open())
	{
		while (std::getline(vertex_file, temp))
			src += temp + '\n';
	}
	else
	{
		std::cerr << "ERROR::LOADSHADERS::VERTEX::FAILED TO OPEN FILE" << std::endl;
		exit(1);
	}
	vertex_file.close();

	GLuint	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	const char	*vertexShaderSource = src.c_str();
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if(!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		exit(1);
	}

	// 2. Fragment Shader
	temp = "";
	src = "";
	path = realpath(PATH, NULL);
	std::ifstream	fragment_file(std::string(path) + "/fragment_core.glsl");
	free(path);
	if (fragment_file.is_open())
	{
		while (std::getline(fragment_file, temp))
			src += temp + '\n';
	}
	else
	{
		std::cerr << "ERROR::LOADSHADERS::FRAGMENT::FAILED TO OPEN FILE" << std::endl;
		exit(1);
	}
	fragment_file.close();

	GLuint	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	const char *fragmentShaderSource = src.c_str();
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if(!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
		exit(1);
	}

	// Create shader program
	GLuint	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if(!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::LINKING::COMPILATION_FAILED\n" << infoLog << std::endl;
		exit(1);
	}

	// Delete linked objects
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	return (shaderProgram);
}

GLuint	loadTexture(std::string path)
{
	GLuint	texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int width, height, nrChannels;
	unsigned char *data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cerr << "Failed to load texture" << std::endl;
		exit(1);
	}
	stbi_image_free(data);
	return (texture);
}

int	main(int ac, char **av)
{
	if (ac != 2)
	{
		std::cerr << "Error: Wrong number of arguments" << std::endl;
		exit(1);
	}
	loadObj(std::string(av[1]));

	GLFWwindow	*win = initWin();

	// OpenGL Options
	// glEnable(GL_DEPTH_TEST);

	// glEnable(GL_CULL_FACE);
	// glCullFace(GL_BACK);
	// glFrontFace(GL_CCW);

	GLuint	shaderProgram = loadShaders();
	GLuint	texture = loadTexture(PATH + std::string("/textures/container.jpg"));

	// Get vertex data into array
    float vertices[] = {
        // positions          // texture coords
		// positions          // colors           // texture coords
		0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
		0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
		-0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
		-0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 

    };
    unsigned int indices[] = {  
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

	// Construct buffer and load vertex array into it
	GLuint	vao, vbo, ebo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// Add vertex components
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), NULL);
	glEnableVertexAttribArray(0);

	// Add color values
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// Add texture coordinates
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);

	// Unbind vao, vbo, ebo
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	while (!glfwWindowShouldClose(win))
	{
		// inputs
		processInput(win);

		// rendering
		glClearColor(0.5f, 0.0f, 0.2f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		// Apply texture
		glBindTexture(GL_TEXTURE_2D, texture);

		// Prepare transformation matrices
		glm::mat4 model = glm::mat4(1.0f);
		glm::mat4 view = glm::mat4(1.0f);
		glm::mat4 proj = glm::mat4(1.0f);
		model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
		proj = glm::perspective(glm::radians(45.0f), (float)WIDTH / (float)HEIGHT, 0.1f, 100.0f);

		// Use shader
		glUseProgram(shaderProgram);

		// Find uniform addresses from vertex shader
		GLuint	modelLoc = 	glGetUniformLocation(shaderProgram, "model");
		GLuint	viewLoc = 	glGetUniformLocation(shaderProgram, "view");
		GLuint	projLoc = 	glGetUniformLocation(shaderProgram, "projection");

		// Bind matrices to uniform addresses
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));

		// Draw on screen
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		//check events and swap buffers
		glfwPollEvents();
		glfwSwapBuffers(win);
	}

	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);
	glDeleteProgram(shaderProgram);

	glfwTerminate();
	return (0);
}
