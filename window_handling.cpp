#include "scop.hpp"

static void	framebuffer_size_callback(GLFWwindow *win, int width, int height)
{
	(void)win;
	glViewport(0, 0, width, height);
}

GLFWwindow	*initWin(void)
{
	// GLFW window initialization
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	# ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // macOS
	# endif

	GLFWwindow	*win = glfwCreateWindow(WIDTH, HEIGHT, "Scop", NULL, NULL);
	if (win == NULL)
	{
		std::cerr << "Error: Failed to create window" << std::endl;
		glfwTerminate();
		exit(1);
	}

	glfwMakeContextCurrent(win);
	glfwSetFramebufferSizeCallback(win, framebuffer_size_callback);

	// Check is GLAD loaded?
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cerr << "Failed to initialize GLAD" << std::endl;
		glfwTerminate();
		exit(1);
	}
	return (win);
}
